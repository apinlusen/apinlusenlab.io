---
title: "About"
description: "I provide valuable insights to organizations seeking to safeguard their digital assets against cyber attacks. I love to help others who share my interests and are committed to advancing the field of cybersecurity"
featured_image: '/images/cybersecurity.jpeg'
menu:
  main:
    weight: 1
---
<!--- 
{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}} 
-->

_Arsène Lupin_ is a fictional gentleman thief and master of disguise created in 1905 by French writer Maurice Leblanc. After watching the Netflix series _Lupin_, I developed a strong affinity for the character as it resonates well with my hacker mindset. In search of something unique,  I conceived _Apin Lusen_ as the inverted counterpart of _Arsène Lupin_ 

In this blog, I document my cybersecurity journey, hoping to provide valuable insights and knowledge for readers. Feel free to learn more about me on [LinkedIn](https://www.linkedin.com/in/nam-pham-44620b1b7/) and [Twitter](https://twitter.com/ApinLusen).

