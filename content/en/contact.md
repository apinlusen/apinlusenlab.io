---
title: Contact
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
---

Feel free to connect with me on [LinkedIn](https://www.linkedin.com/in/nam-pham-44620b1b7/) or [Twitter](https://twitter.com/ApinLusen) and share your thoughts. I look forward to hearing from you.
