---
date: 2023-07-16
description: "A consise, action-oriented guide"
featured_image: "/images/OSCP-cert.png"
tags: []
title: "A simple guide for you to pass OSCP with ease"
disable_share: false
---

Do not let the title mislead you. Obviously, the difficulty level of the OSCP exam is subjective, but it definitely contains some rabbit holes and can be a mental challenge. Therefore, you need to be mentally tough, disciplined, and well-prepared in order to pass this exam with ease.

There are tons of study guides for OSCP out there. I used to read a lot of those before taking the exam. And that was a mistake. Why? Because they made me overthink and spend less time taking action. Moreover, many of them lack the most important things to do. Therefore, I will fill in the gaps in this guide and keep it concise and action-oriented.

First and foremost, you gotta feel comfortable working on the terminal. When preparing for the OSCP exam, the terminal is your home as you spend most of the time working on it. Make sure your home is tidy and well-organized at all times. You have to use a tool to do that, so choose one. I recommend tmux; it requires a steep learning curve, but it's worth it. Do not let complex systems clutter your terminal with rabbit holes and unimportant information. You have been warned about this.

The second thing is to be mentally prepared. You have to get used to failure. Go on Hackthebox, spawn some easy-medium machines, and fail. Keep failing, and then 1-2 days later, read the write-ups and realize how naive you were. Most of the time, the solutions were right in front of you, but you overlooked them. After reading the write-ups, solve the machine, wait for 2-3 days, and solve the same machine again. Take note of what you missed on the first attempt. That is a beautiful growth process. There will be rabbit holes in the exam, and you will overlook the hints. This is the only way to prepare for them.

The last thing is to be brave and work hard. Just start PEN-200 when you understand the fundamentals (networking, web applications, Bash, Python, etc.). It sounds obvious, but I still want to repeat it: the best way to prepare for the OSCP exam is by finishing the PEN-200 materials. I cannot imagine that a lot of people did not finish the PEN-200 materials but still took the exam and failed. It is confusing. Make sure you finish the "necessary" labs; you will know what they are. If you think you are too "busy" right now to start the course, just start it, and you will find the time. You will soon realize that you are not busy; you have just mindlessly filled up your days with unimportant tasks.

That's it. I love to keep things short, but important and action-oriented. Start doing what you have to do; you have been guided.