---
date: 2023-10-01
description: "A short introduction to BChecks and use case of Web Cache Deception"
featured_image: "/images/WCD-Bcheck.jpeg"
tags: []
title: "The power of BChecks: quickly turning security research into profits"
disable_share: false
---

BChecks are custom scan checks that can be easily created and imported. Most importantly, it is an extension to Burp Scanner so that makes the testing workflow extremely efficient. This is one of the reason why BChecks is preferred over the Nuclei on many occasions. In this article, I will showcase an example of using BCheck for "Web Cache Deception" attack

A quick brief on Web Cache Deception: It exploits misconfiguration in caching servers by making them cache a user's sensitive information and then the response is availabe to attackers. In most cases, this type of attack requires user interaction.

Regarding the title, [this is the research](https://seclab.nu/static/publications/sec2020wcd.pdf) about Web Cache Deception that we will delve into, quickly turning it into a BCheck script, then possibly earning some bounties. 

Overall, this is what the script looks like in the BS Code Editor:
[BCheck screenshot](/images/WCD-Bcheck-screenshot.png) 

```
1 metadata:
2   language: v1-beta
3   name: "Web cache deception"
4   description: "Scan requests that contain sesitive information"
5   author: "Nam Pham"
6   tags: "wcd"
```
The first section lists metadata about the script, with the 'tags' being especially important for classifying different scripts.

```
8   run for each:
9	cache_extension = ".avif", ".class", ".css", ".jar", ".js", ".jpg", ".jpeg", ".gif", ".ico", ".png", ".bmp", ".pict",
10           ".csv", ".doc", ".docx", ".xls", ".xlsx", ".ps", ".pdf", ".pls", ".ppt", ".pptx", ".tif", ".tiff", ".ttf", 
11           ".otf", ".webp", ".woff", ".woff2", ".svg", ".svgz", ".eot", ".eps", ".ejs", ".swf", ".torrent", ".midi", ".mid",
12           ";.avif", ";.class", ";.css", ";.jar", ";.js", ";.jpg", ";.jpeg", ";.gif", ";.ico", ";.png", ";.bmp", ";.pict",
13           ";.csv", ";.doc", ";.docx", ";.xls", ";.xlsx", ";.ps", ";.pdf", ";.pls", ";.ppt", ";.pptx", ";.tif", ";.tiff", ";.ttf", 
14           ";.otf", ";.webp", ";.woff", ";.woff2", ";.svg", ";.svgz", ";.eot", ";.eps", ";.ejs", ";.swf", ";.torrent", ";.midi", ";.mid"
15	path_confusion = "/", "%0A", "%3B", "%23", "%3F"		
```
The control flow, labeled 'run for each,' declares array variables that can be iterated over. When the variable is called, the check runs once for each item in the array.

As mentioned in the research, caching servers can be misconfigured to automatically store responses for requests that end with static extensions. Therefore, we need to iterate through all popular static extensions.

Moreover, the Web Cache Deception attack also utilizes path confusion techniques for crafting URLs that reference non-existent file names.

```
17 given request then  
18	send request called cache:
19	    method: "GET"
20	    appending path: `{path_confusion}lzp{cache_extension}`
21       
22	    if {cache.response.body} is {base.response.body} then
23	    	send request called hacker:
24	    		`GET {base.request.url.path}{path_confusion}lzp{cache_extension} HTTP/2
25	    		Host: {base.response.url.host}
26	    		X-Ignore`
27	    	if {hacker.response.body} is {base.response.body} then
28	    		report issue:
29	    		    severity: medium
30	    		    confidence: certain
31	    	end if
32	    end if
```

The actions and conditions prompt Burp Scanner to perform a specific action based on a predefined condition.

In practice, we need to find a response that contains sensitive information valuable to hackers. Then, by combining path confusion techniques and adding non-existent file names with static extensions, we check if the server caches the chosen response.

If the response is cached, we would send another request that does not contain a valid cookie to simulate a hacker without proper authorization. If we still receive the same response, which includes sensitive information, then we have identified a Web Cache Deception vulnerability.

Research is complete, the script is ready, and now it's your turn to scan potential requests with it. You may find [this guide from PortSwigger](https://portswigger.net/burp/documentation/desktop/automated-scanning/bchecks) helpful for running scans with BCheck. I'll leave [this report, which earned a reward of $150](https://hackerone.com/reports/593712) as motivation for you.

That’s it. Short and easy. Now it’s your turn to get creative with BChecks and integrate those scripts into your workflow. I challenge readers to create BCheck scripts for Host Header Injection attacks. Those scripts would be much more complex, but that also means more fun, doesn’t it? If you encounter any difficulties while creating those scripts, feel free to contact me for a discussion :)